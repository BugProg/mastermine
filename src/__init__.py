from random import choice

#Make the relation between the color and the html color
colorsDictionary = {
    "Blue": "#0078ff", #Blue
    "Green": "#12c900", #green
    "Red": "#ff0000", #red
    "Pink": "#ff54b9", #pink
    "White": "#ffffff", #white
    "Orange": "#ff8300", #orange
    "Black" : "#000000" #Black
}

def init(colors = colorsDictionary):
    """
    choisi 4 elements au hasard dans une liste

    parametre:
    --> colors: liste de colors qu'on veut pouvoir avoir

    sortie:
    --> colours: liste de 4 colors choisit aléatoirement
    """
    lla = ['Green', 'Green', 'Pink', 'Green']
    return lla#[choice(list(colors.keys())) for i in range(4)]

#Debug
if __name__ == '__main__':
    print("Colors returned : ",init(colorsDictionary))