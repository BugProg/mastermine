#!/usr/bin/env python3
# coding: utf-8

#In Game :
#Red piece : Right color
#Black piece : Right color and position

#Import Gtk
from gi.repository import Gtk
from playsound import playsound
from random import shuffle
import __init__

#Store the color defined by the user
user_color_selected = {
    "first_input": "",
    "second_input": "",
    "third_input": "",
    "fourth_input": ""
}

remaining_life = 10 #The user's life
colorToFind = __init__.init() #The different colors to find

def color_check (colorsToCheck):
    global colorToFind
    pieces = [builder.get_object('first_piece'), builder.get_object('second_piece'), builder.get_object('third_piece'), builder.get_object('fourth_piece')]

    colorPieces = ["White"]* 4 #A list to stock the color to show for the pieces
    print ("Color to find : ", colorToFind)#Debug
    
    #Process for the Black pieces
    for i in range (len (colorToFind)):
        if colorsToCheck[i].get_text() in colorToFind:
            if colorsToCheck[i].get_text() == colorToFind[i]: #Check if the index of each list is the same
                colorPieces [i] = "Black"
                print ("Good position and color for round :", i) #Debug

    colorToFindTMP = []
    for i in range ( len (colorToFind)):
        colorToFindTMP.append (colorToFind[i])
   
    #Process for the Red pieces
    for i in range ( len (colorToFind)):
        for y in range (len (colorsToCheck)):
            print ("TMP ",colorToFindTMP,"i: ",i," y :",y)
            if colorsToCheck[y].get_text() == colorToFindTMP[i] and colorPieces[y] == "White":
                colorPieces [y] = "Red"
                colorToFindTMP[i] = "Removed"
                print ("Only good color for round y - i:", y," - ", i) #Debug

    print ("Pieces colors (before shuffle) : ", colorPieces) #Debug
    #shuffle (colorPieces)
    print ("Pieces colors (after shuffle) : ", colorPieces) #Debug
 

    for i in range (len (colorPieces)):
        pieces[i].set_markup('<span color="{}" size="70000"><b>.</b></span>'.format (__init__.colorsDictionary[colorPieces[i]]))

    if len (set (colorPieces)) == 1 and colorPieces [0] == "Black": #User win
        builder.get_object('next_btn').set_sensitive(False) #Disable Next button
        builder.get_object('user_life').set_markup('<span size="50000"><b>{}</b></span>'.format("You Won !!"))
        play_sounds ("Won")


def life_manager():
    '''
    DESCRIPTION :
        Manage the life and show it.

    PARAMETER :
        The label to allow to modifie it
        
    RETURN :
        None
    '''
    global remaining_life
    label = builder.get_object('user_life')
    remaining_life -= 1
    if remaining_life > -1:
        label.set_markup('<span size="50000"><b>{}</b></span>'.format(remaining_life))
    else:
        #The user lost
        builder.get_object('next_btn').set_sensitive(False) #Disable Next button
        label.set_markup('<span size="50000"><b>{}</b></span>'.format("You lost"))
        play_sounds ("lost")

def update_colors (color):
    '''
    DESCRIPTION :
        Update rounds color

    PARAMETER :
        -The color for update
        
    RETURN :
        None
    '''
    rounds = [builder.get_object('label_first_btn'), builder.get_object('label_second_btn'), builder.get_object('label_third_btn'), builder.get_object('label_fourth_btn')]
    
    for i in range (len (rounds)):
        rounds[i].set_markup('<span color="{}" size="100000"><b>O</b></span>'.format(__init__.colorsDictionary[color[i].get_text()]))

def inputs_check (button):
    '''
    DESCRIPTION :
        Check each input selected by user.
            Show an error if the input is not correct or continue if all is right.

    PARAMETER :
        -The button parameters (glade) : not used
        
    RETURN :
        None
    '''
    error = 0
    inputs = [builder.get_object('first_input'), builder.get_object('second_input'), builder.get_object('third_input'), builder.get_object('fourth_input')]

    print ("Colors selected by user:") #Debug
    for i in range (len (inputs)):
        print ("Color {} : {}".format (i, inputs[i].get_text())) #Debug
        if inputs[i].get_text() == "": #Check if a color is defined
            print ("Error on button {}, color not defined.".format(i)) #Debug
            show_error ('You must fill all inputs !!')
            error = 1
            break
    if error == 0:
        show_error ('') #Clear error
        update_colors (inputs)
        color_check (inputs)
        life_manager()


def show_error (message):
    '''
    DESCRIPTION :
        Show potential error on the main window

    PARAMETER :
        -The message to show

    RETURN :
        None
    '''
    label = builder.get_object('error_label')
    label.set_markup('<span color="#ff0000"><b>{}</b></span>'.format(message))


def play_sounds (title):
    '''
    DESCRIPTION :
        Play sound

    PARAMETER :
        -The name of sound to play

    RETURN :
        None
    '''
    try:
        print ("Play sound : ",title.get_label()) #Debug
    except:
        print ("Play sound : ",title) #Debug

    if title == 'lost':
        playsound('asset/lost.mp3')
    
    elif title == 'Won':
        playsound ('asset/won.mp3')

    elif title.get_label() == 'Tuto':
        playsound('asset/tuto.mp3')


builder = Gtk.Builder() #Init the app
builder.add_from_file('window.ui') # Init the UI from file

window = builder.get_object('MastermineWindow')#Init the main window
window.connect('delete-event', Gtk.main_quit) #Add destroy winodow button

# The handler
handler = {
#     ID (Glade) function
    'play_tuto': play_sounds,
    'check_colors': inputs_check
}
builder.connect_signals(handler)

if __name__ == '__main__':
    #Start the App
    window.show_all()
    Gtk.main()
